package edu.tsupko.stream.intermediate;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
		s.filter(x -> x.startsWith("m")).forEach(System.out::println);
		System.out.println();

		s = Stream.of("duck", "duck", "duck", "goose");
		s.distinct().forEach(System.out::print);
		System.out.println();
		System.out.println();

		Stream<Integer> stream = Stream.iterate(1, n -> n + 1);
		stream.skip(5).limit(2).forEach(System.out::print);
		System.out.println();
		System.out.println();

		s = Stream.of("monkey", "gorilla", "bonobo");
		s.map(String::length).forEach(System.out::print);
		System.out.println();
		System.out.println();

		List<String> zero = List.of();
		var one = List.of("Bonobo");
		var two = List.of("Mama Gorilla", "Baby Gorilla");
		Stream<List<String>> animals = Stream.of(zero, one, two);
		animals.flatMap(Collection::stream).forEach(System.out::println);
		System.out.println();

		s = Stream.of("brown-", "bear-");
		s.sorted(Comparator.reverseOrder()).forEach(System.out::print);
		System.out.println();
	}
}
