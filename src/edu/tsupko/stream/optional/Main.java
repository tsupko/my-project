package edu.tsupko.stream.optional;

import java.util.Optional;

public class Main {
	public static void main(String[] args) {
		Optional<Double> optional = average(90, 100);
		optional.ifPresent(System.out::println);
		optional = average();
		optional.ifPresent(System.out::println);

		Object value = new Object();
		Optional o = (value == null) ? Optional.empty() : Optional.of(value);
		value = null;
		o = Optional.ofNullable(value);
	}

	public static Optional<Double> average(int... scores) {
		if (scores.length == 0) {
			return Optional.empty();
		}
		int sum = 0;
		for (int score : scores) {
			sum += score;
		}
		return Optional.of((double) sum / scores.length);
	}
}
