package edu.tsupko.stream.terminal;

import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		// count()
		// min(), max()
		// findAny(), findFirst()
		// allMatch(), anyMatch(), noneMatch()
		// forEach()
		// reduce()
		// collect()

		Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
		System.out.println(s.count()); // 3

		s = Stream.of("monkey", "ape", "bonobo");
		Optional<String> min = s.min((s1, s2) -> s1.length() - s2.length());
		min.ifPresent(System.out::println);

		s = Stream.of("monkey", "gorilla", "bonobo");
		Stream<String> infinite = Stream.generate(() -> "chimp");

		s.findAny().ifPresent(System.out::println);
		infinite.findAny().ifPresent(System.out::println);

		var list = List.of("monkey", "2", "chimp");
		infinite = Stream.generate(() -> "chimp");
		Predicate<String> predicate = x -> Character.isLetter(x.charAt(0));

		System.out.println(list.stream().anyMatch(predicate)); // true
		System.out.println(list.stream().allMatch(predicate)); // false
		System.out.println(list.stream().noneMatch(predicate)); // false
		System.out.println(infinite.anyMatch(predicate)); // true

		Stream<String> stream = Stream.of("w", "o", "l", "f");
		var set = stream.collect(Collectors.toCollection(TreeSet::new));
		System.out.println(set);
	}
}
