package edu.tsupko.stream;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		var list = List.of("Toby", "Anna", "Leroy", "Alex");
		List<String> filtered = list.stream()
				.filter(name -> name.length() == 4)
				.sorted()
				.limit(2)
				.collect(Collectors.toList());
		System.out.println(filtered);
		System.out.println();

		long count = Stream.of("goldfish", "finch")
				.filter(s -> s.length() > 5)
				.toList()
				.size();
		System.out.println(count);
	}
}
