package edu.tsupko.homework;

public class Utils {

	public static final int BASE = 10;

	private Utils() {
		throw new UnsupportedOperationException("Utility classes should not be instantiated");
	}

	public static boolean isEven(int number) {
		return number % 2 == 0;
	}

	public static boolean isDigitSumEven(int number) {
		var sum = 0;
		number = Math.abs(number);
		while (number != 0) {
			sum += number % BASE;
			number /= BASE;
		}
		return isEven(sum);
	}
}
