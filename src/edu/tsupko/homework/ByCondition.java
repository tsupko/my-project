package edu.tsupko.homework;

@FunctionalInterface
public interface ByCondition {
	boolean isOk(int number);
}
