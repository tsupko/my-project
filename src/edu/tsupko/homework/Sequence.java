package edu.tsupko.homework;

import java.util.Arrays;

public class Sequence {
	public static int[] filter(int[] array, ByCondition condition) {
		int count = 0;
		for (int index = 0; index < array.length; ++index) {
			if (condition.isOk(array[index])) {
				array[count++] = array[index];
			}
		}
		return Arrays.copyOf(array, count);
	}
}
