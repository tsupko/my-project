package edu.tsupko.homework;

public class Main {
	public static void main(String[] args) {
		var sb = new StringBuilder();
		var s = sb
				.append("Пупкин")
				.append(" ")
				.append("Вася")
				.append(" ")
				.append("Варфаламеевич")
				.toString();
		System.out.println(s);

		var passport = "62\t84 741648";
		String[] split = passport.split("\\s+");
		var series = String.join(" ", split[0], split[1]);
		var number = split[2];
		System.out.println(series + " " + number);
	}
}
