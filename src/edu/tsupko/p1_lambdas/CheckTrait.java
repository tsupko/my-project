package edu.tsupko.p1_lambdas;

@FunctionalInterface
public interface CheckTrait {
	boolean test(Animal animal);
}
