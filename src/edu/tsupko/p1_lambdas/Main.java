package edu.tsupko.p1_lambdas;

public class Main {
	public static void main(String[] args) {
		Animal[] animals = new Animal[4];
		animals[0] = new Animal("fish", false, true);
		animals[1] = new Animal("kangaroo", true, false);
		animals[2] = new Animal("rabbit", true, false);
		animals[3] = new Animal("turtle", false, true);

		print(animals, Animal::canHop);
		System.out.println();
		print(animals, a -> a.canSwim());
	}

	private static void print(Animal[] animals, CheckTrait checker) {
		for (Animal animal : animals) {
			if (checker.test(animal)) {
				System.out.println(animal + " ");
			}
		}
	}
}
