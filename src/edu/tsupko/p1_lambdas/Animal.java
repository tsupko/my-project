package edu.tsupko.p1_lambdas;

public record Animal(String species, boolean canHop, boolean canSwim) {
}
