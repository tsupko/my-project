package edu.tsupko.util;

public class Main {
	public static void main(String[] args) {
		B[] bs = {new A(), new C()};
		for (B b1 : bs) {
			b1.print();
		}
	}
}
