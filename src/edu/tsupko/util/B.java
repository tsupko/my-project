package edu.tsupko.util;

public abstract class B {
	protected long d;
	protected boolean e;
	protected StringBuilder f;

	protected int h = 5;

	public B() {

	}

	public B(long d) {
		this.d = d;
	}

	public B(long d, boolean e) {
		this(d);
		this.e = e;
	}

	public B(long d, boolean e, StringBuilder f) {
		this(d, e);
		this.f = f;
	}

	public long getD() {
		return d;
	}

	public void setD(long d) {
		this.d = d;
	}

	public boolean isE() {
		return e;
	}

	public void setE(boolean e) {
		this.e = e;
	}

	public StringBuilder getF() {
		return f;
	}

	public void setF(StringBuilder f) {
		this.f = f;
	}

	public B b(boolean e) {
		this.e = e;
		return this;
	}

	public abstract void print();
}
