package edu.tsupko.util;

public class C extends B implements I {
	@Override
	public void print() {
		System.out.println("From class C");
	}

	@Override
	public void printFromInterface() {
		System.out.println("From class C, interface I");
	}
}
