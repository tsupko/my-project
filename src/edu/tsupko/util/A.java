package edu.tsupko.util;

public final class A extends B implements I {
	private int a;
	private double b;
	private String c;

	private int h;

	public A() {

	}

	public A(int a, double b, String c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public int getH() {
		return h;
	}

	public int getA() {
		return a;
	}

	public double getB() {
		return b;
	}

	public String getC() {
		return c;
	}

	@Override
	public String toString() {
		return "edu.tsupko.util.A{" + "a=" + a +
				", b=" + b +
				", c='" + c + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		A a1 = (A) o;

		if (a != a1.a) {
			return false;
		}
		if (Double.compare(a1.b, b) != 0) {
			return false;
		}
		return c.equals(a1.c);
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = a;
		temp = Double.doubleToLongBits(b);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + c.hashCode();
		return result;
	}

	@Override
	public void print() {
		System.out.println("From class A");
	}

	@Override
	public void printFromInterface() {
		System.out.println("From class A, interface I");
	}
}
