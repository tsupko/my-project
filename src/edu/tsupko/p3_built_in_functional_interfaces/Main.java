package edu.tsupko.p3_built_in_functional_interfaces;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
	public static void main(String[] args) {
		Predicate<String> egg = s -> s.contains("egg");
		Predicate<String> brown = s -> s.contains("brown");

		Predicate<String> brownEggs = s -> s.contains("egg") && s.contains("brown");
		Predicate<String> otherEggs = s -> s.contains("egg") && !s.contains("brown");

		Predicate<String> brownEggs1 = egg.and(brown);
		Predicate<String> otherEggs1 = egg.and(brown.negate());

		Consumer<String> c1 = x -> System.out.print("1: " + x);
		Consumer<String> c2 = x -> System.out.println(",2: " + x);

		Consumer<String> combined = c1.andThen(c2);
		combined.accept("Annie");

		Function<Integer, Integer> before = x -> x + 1;
		Function<Integer, Integer> after = x -> x * 2;

		Function<Integer, Integer> combinedFunction = after.compose(before);
		System.out.println(combinedFunction.apply(3)); // 7 или 8?
	}
}
