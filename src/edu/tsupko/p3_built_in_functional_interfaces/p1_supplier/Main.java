package edu.tsupko.p3_built_in_functional_interfaces.p1_supplier;

import java.time.LocalDate;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

public class Main {
	public static void main(String[] args) {
		Supplier<LocalDate> s1 = LocalDate::now;
		Supplier<LocalDate> s2 = () -> LocalDate.now();

		LocalDate d1 = s1.get();
		LocalDate d2 = s2.get();
		System.out.println(d1);
		System.out.println(d2);

		Supplier<StringBuilder> s3 = StringBuilder::new;
		Supplier<StringBuilder> s4 = () -> new StringBuilder();
		System.out.println(s3.get());
		System.out.println(s4.get());

		BooleanSupplier b1 = () -> true;
		BooleanSupplier b2 = () -> Math.random() > .5;

		System.out.println(b1.getAsBoolean());
		System.out.println(b2.getAsBoolean());
	}
}
