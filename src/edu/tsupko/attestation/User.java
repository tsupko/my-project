package edu.tsupko.attestation;

public class User {
	private int id;
	private String firstName;
	private String lastName;
	private int age;
	private boolean employed;

	public User(int id, String firstName, String lastName, int age, boolean employed) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.employed = employed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isEmployed() {
		return employed;
	}

	public void setEmployed(boolean employed) {
		this.employed = employed;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		User user = (User) o;

		if (id != user.id) {
			return false;
		}
		if (age != user.age) {
			return false;
		}
		if (employed != user.employed) {
			return false;
		}
		if (!firstName.equals(user.firstName)) {
			return false;
		}
		return lastName.equals(user.lastName);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + firstName.hashCode();
		result = 31 * result + lastName.hashCode();
		result = 31 * result + age;
		result = 31 * result + (employed ? 1 : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("User{");
		sb.append("id=").append(id);
		sb.append(", firstName='").append(firstName).append('\'');
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", age=").append(age);
		sb.append(", employed=").append(employed);
		sb.append('}');
		return sb.toString();
	}
}
