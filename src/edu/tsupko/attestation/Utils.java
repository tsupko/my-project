package edu.tsupko.attestation;

public class Utils {
	public static final String PREFIX = "C:\\Users\\Admin\\IdeaProjects\\my-project\\src\\edu\\tsupko\\";
	public static final String FILE = PREFIX + "\\attestation\\users";
	public static final String OUTPUT = PREFIX + "\\io\\output";

	private Utils() {
		throw new UnsupportedOperationException("Utility classes should not be instantiated");
	}

	public static User parseUser(String line) {
		String[] split = line.split("\\|");
		return new User(Integer.parseInt(split[0]),
				split[1],
				split[2],
				Integer.parseInt(split[3]),
				Boolean.parseBoolean(split[4]));
	}
}
