package edu.tsupko.attestation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static edu.tsupko.attestation.Utils.FILE;

public class Main {

	public static void main(String[] args) {
		try (var stream = Files.lines(Path.of(FILE))) {
			List<User> users = stream
					.map(Utils::parseUser)
					.toList();
			System.out.println(users);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
