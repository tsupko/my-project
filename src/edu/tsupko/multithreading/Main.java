package edu.tsupko.multithreading;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main {
	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		try {
			System.out.println("begin");
			for (var power = 1; power < 11; power++) {
				int finalPower = power;
				Future<?> result = service.submit(() -> findAllPrimesLessThan(1 << finalPower));
				Object o = result.get(30, TimeUnit.SECONDS);
				if (o instanceof int[] array) {
					System.out.println(Arrays.toString(array));
				}
			}
			System.out.println("end");
		} catch (ExecutionException | InterruptedException | TimeoutException e) {
			throw new RuntimeException(e);
		} finally {
			service.shutdown();
		}
	}

	private static int[] findAllPrimesLessThan(int bound) {
		long start = System.nanoTime();
		var primes = new int[bound];
		var count = 0;
		primes[count++] = 2;
		for (int number = 3; number <= bound; number += 2) {
			if (isPrime(number)) {
				primes[count++] = number;
			}
		}
		System.out.printf("Thread %s: %.1f mcs%n", Thread.currentThread().getName(), (System.nanoTime() - start) / 1e3);
		return Arrays.copyOf(primes, count);
	}

	private static boolean isPrime(long number) {
		for (int divisor = 2; divisor <= Math.sqrt(number); divisor++) {
			if (number % divisor == 0) {
				return false;
			}
		}
		return true;
	}
}
