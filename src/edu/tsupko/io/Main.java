package edu.tsupko.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static edu.tsupko.attestation.Utils.FILE;
import static edu.tsupko.attestation.Utils.OUTPUT;

public class Main {
	public static void main(String[] args) throws IOException {
		Path file = Path.of(FILE);
		Path output = Path.of(OUTPUT);
		Main.copyPathAsLines(file, output);
		int pipeIndex = args[0].indexOf('|');
		String id = args[0].substring(0, pipeIndex);
		int idAsInt = Integer.parseInt(id);
	}

	void copyStream(InputStream in, OutputStream out) throws IOException {
		int b;
		while ((b = in.read()) != -1) {
			out.write(b);
		}
	}

	void copyStream(Reader in, Writer out) throws IOException {
		int b;
		while ((b = in.read()) != -1) {
			out.write(b);
		}
	}

	void copyStreamUsingBuffer(InputStream in, OutputStream out) throws IOException {
		int batchSize = 1_024;
		var buffer = new byte[batchSize];
		int lengthRead;
		while ((lengthRead = in.read(buffer, 0, batchSize)) > 0) {
			out.write(buffer, 0, lengthRead);
			out.flush();
		}
	}

	void copyTextFile(File src, File dest) throws IOException {
		try (var reader = new BufferedReader(new FileReader(src));
			 var writer = new PrintWriter(new FileWriter(dest))) {
			String line;
			while ((line = reader.readLine()) != null) {
				writer.println(line);
			}
		}
	}

	void copyPathAsString(Path input, Path output) throws IOException {
		String string = Files.readString(input);
		Files.writeString(output, string);
	}

	void copyPathAsBytes(Path input, Path output) throws IOException {
		byte[] bytes = Files.readAllBytes(input);
		Files.write(output, bytes);
	}

	static void copyPathAsLines(Path input, Path output) throws IOException {
		List<String> lines = Files.readAllLines(input);
		Files.write(output, lines);
	}

	void copyPath(Path input, Path output) throws IOException {
		try (var reader = Files.newBufferedReader(input);
			 var writer = Files.newBufferedWriter(output)) {
			String line;
			while ((line = reader.readLine()) != null) {
				writer.write(line);
				writer.newLine();
			}
		}
	}
}
