package edu.tsupko.p2_method_references.k2_second_kind;

@FunctionalInterface
public interface StringChecker {
	boolean check();
}
