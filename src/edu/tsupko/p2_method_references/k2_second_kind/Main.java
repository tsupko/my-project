package edu.tsupko.p2_method_references.k2_second_kind;

public class Main {
	public static void main(String[] args) {
		var str = "Zoo";
		StringStart methodRef = str::startsWith;
		StringStart lambda = anything -> str.startsWith(anything);

		System.out.println(methodRef.beginningCheck("A"));
		System.out.println(lambda.beginningCheck("Z"));

		var emptyStr = "";
		StringChecker methodRef1 = emptyStr::isEmpty;
		StringChecker lambda1 = () -> emptyStr.isEmpty();

		System.out.println(methodRef1.check());
	}
}
