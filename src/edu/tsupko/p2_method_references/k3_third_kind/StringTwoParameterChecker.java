package edu.tsupko.p2_method_references.k3_third_kind;

@FunctionalInterface
public interface StringTwoParameterChecker {
	boolean check(String text, String prefix);
}
