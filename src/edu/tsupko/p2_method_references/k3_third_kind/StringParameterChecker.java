package edu.tsupko.p2_method_references.k3_third_kind;

@FunctionalInterface
public interface StringParameterChecker {
	boolean check(String text);
}
