package edu.tsupko.p2_method_references.k3_third_kind;

public class Main {
	public static void main(String[] args) {
		StringParameterChecker methodRef = String::isEmpty;
		StringParameterChecker lambda = s -> s.isEmpty();

		System.out.println(methodRef.check("Zoo"));
		System.out.println(lambda.check(""));

		StringTwoParameterChecker methodRef1 = String::startsWith;
		StringTwoParameterChecker lambda1 = (s, p) -> s.startsWith(p);

		System.out.println(methodRef1.check("Zoo", "A"));
		System.out.println(lambda1.check("Zoo", "Z"));
	}
}
