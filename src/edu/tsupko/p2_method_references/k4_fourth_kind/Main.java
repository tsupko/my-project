package edu.tsupko.p2_method_references.k4_fourth_kind;

public class Main {
	public static void main(String[] args) {
		EmptyStringCreator methodRef = String::new;
		EmptyStringCreator lambda = () -> new String();

		var myString = methodRef.create();
		System.out.println(myString.equals("Snake")); // false
		System.out.println("!!!" + myString + "!!!");

		StringCopier methodRef1 = String::new;
		StringCopier lambda1 = x -> new String(x);

		var myString1 = methodRef1.copy("Zebra");
		System.out.println(myString1.equals("Zebra"));
	}
}
