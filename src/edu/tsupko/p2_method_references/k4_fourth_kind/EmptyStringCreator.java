package edu.tsupko.p2_method_references.k4_fourth_kind;

@FunctionalInterface
public interface EmptyStringCreator {
	String create();
}
