package edu.tsupko.p2_method_references.k1_first_kind;

@FunctionalInterface
public interface Converter {
	long round(double num);
}
