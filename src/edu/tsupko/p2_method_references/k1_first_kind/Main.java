package edu.tsupko.p2_method_references.k1_first_kind;

public class Main {
	public static void main(String[] args) {
		Converter methodRef = Math::round;
		Converter lambda = anything -> Math.round(anything);
		System.out.println(methodRef.round(100.1));
		System.out.println(lambda.round(100.1));
	}
}
