package edu.tsupko.homework;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SequenceTest {

	private static Stream<Arguments> supplyParametersForConditionIsEven() {
		return Stream.of(
				Arguments.of(new int[]{-292_389_122, 322_842, 1_111_243, 5_111_011}, new int[]{-292_389_122, 322_842}),
				Arguments.of(new int[]{292_921, 23_679, 235, -29_228}, new int[]{-29_228}),
				Arguments.of(new int[]{2_952_095, 7_433, 0, -333_933, 20_202, 502_385}, new int[]{0, 20_202})
		);
	}

	private static Stream<Arguments> supplyParametersForConditionIsDigitSumEven() {
		return Stream.of(
				Arguments.of(new int[]{-292_389_122, 322_842, 1_111_243, 5_111_011}, new int[]{-292_389_122, 5_111_011}),
				Arguments.of(new int[]{292_921, 23_679, 235, -29_228}, new int[]{235}),
				Arguments.of(new int[]{2_952_095, 7_433, 0, -333_933, 20_202, 502_385}, new int[]{2_952_095, 0, -333_933, 20_202})
		);
	}

	@ParameterizedTest
	@MethodSource("supplyParametersForConditionIsEven")
	@DisplayName("проверка чётности элементов массива")
	void filterByConditionIsEven(int[] array, int[] expected) {
		int[] actual = Sequence.filter(array, Utils::isEven);
		assertArrayEquals(expected, actual,
				String.format("Массивы не равны:%n\tожидали %s, получили %s%n",
						Arrays.toString(expected), Arrays.toString(actual)));
	}

	@ParameterizedTest
	@MethodSource("supplyParametersForConditionIsDigitSumEven")
	@DisplayName("проверка чётности суммы цифр элементов массива")
	void filterByConditionIsDigitSumEven(int[] array, int[] expected) {
		int[] actual = Sequence.filter(array, Utils::isDigitSumEven);
		assertArrayEquals(expected, actual,
				String.format("Массивы не равны:%n\tожидали %s, получили %s%n",
						Arrays.toString(expected), Arrays.toString(actual)));
	}
}
