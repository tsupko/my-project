package edu.tsupko.stc_22_60.sub02_declaring_multiple_variables;

public class Main {
	public static void main(String[] args) {
//        int num, String value; // компилируется?
	}

	// сколько переменных объявлено и инициализировано?
	void sandFence() {
		String s1, s2;
		String s3 = "yes", s4 = "no";
	}

	// сколько переменных объявлено и инициализировано?
	void paintFence() {
		int i1, i2, i3 = 0;
	}
}
