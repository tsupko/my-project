package edu.tsupko.stc_22_60.sub05_defining_text_blocks;

public class Main {
	public static void main(String[] args) {
		String eyeTest = "\"Java Study Guide\"\n   by Scott & Jeanne";

		String textBlock = """
				"Java Study Guide"
				   by Scott & Jeanne""";

		System.out.println("eyeTest = \n" + eyeTest);
		System.out.println("textBlock = \n" + textBlock);
	}
}
