package edu.tsupko.stc_22_60.sub02_using_reference_types;

public class ReferenceTypes {
    public static void main(String[] args) {
        String greeting; // тип и имя ссылочной переменной
        greeting = "How are you?"; // ссылка указывает на новый объект класса `String`

        String string = "I'm fine. Thanks! And you?";
        greeting = string; // ссылка указывает на тот же объект типа `String`, на который указывает переменная `string`
    }
}
