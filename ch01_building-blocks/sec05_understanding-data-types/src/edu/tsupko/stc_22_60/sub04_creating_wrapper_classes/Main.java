package edu.tsupko.stc_22_60.sub04_creating_wrapper_classes;

public class Main {
	public static void main(String[] args) {
		int primitive = Integer.parseInt("123");
		Integer wrapper = Integer.valueOf("123");

		Double apple = Double.valueOf("200.99");
		System.out.println(apple.byteValue()); //   ?
		System.out.println(apple.intValue()); //    ?
		System.out.println(apple.doubleValue()); // ?

		int a = -23;
		int b = 91;
		int min = Integer.min(a, b);
		System.out.println("min = " + min);
		int max = Integer.max(a, b);
		System.out.println("max = " + max);
		int sum = Integer.sum(a, b);
		System.out.println("sum = " + sum);
	}
}
