package edu.tsupko.stc_22_60.sub01_creating_local_variables;

public class LocalVariables {
	public int notValid() {
		int y = 10;
		int x;
//        int reply = x + y;
//        return reply;
		return 0;
	}

	public int valid() {
		int y = 10;
		int x; // объявление локальной переменной
		x = 3; // инициализация локальной переменной
		int z; // z объявлена, но нигде не инициализируется
		int reply = x + y;
		return reply;
	}

	public void findAnswer(boolean check) {
		int answer;
		int otherAnswer;
		int onlyOneBranch;
		if (check) {
			onlyOneBranch = 1;
			answer = 1;
		} else {
			answer = 2;
		}
		System.out.println(answer);
//        System.out.println(onlyOneBranch); // компилируется?
	}
}
