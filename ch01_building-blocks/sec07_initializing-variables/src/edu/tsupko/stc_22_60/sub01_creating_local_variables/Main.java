package edu.tsupko.stc_22_60.sub01_creating_local_variables;

public class Main {
	public static void main(String[] args) {
		final int y = 10;
		int x = 20;
//        y = x + 10; // компилируется?

		final int[] favoriteNumbers = new int[10];
		favoriteNumbers[0] = 10;
		favoriteNumbers[1] = 20;
//        favoriteNumbers = null; // компилируется?
	}
}
