package edu.tsupko.stc_22_60.sub04_inferring_the_type_with_var;

public class ExploringTypeInference {
	public void reassignment() {
		var number = 7;
		number = 4;
//      number = "five"; // компилируется?
	}

	public void doesThisCompile(boolean check) {
//        var question;
//        question = 1;
//        var answer;
//        if (check) {
//            answer = 2;
//        } else {
//            answer = 3;
//        }
//        System.out.println(answer);
	}

	public void twoTypes() {
//        int a, var b = 3;
//        var n = null;
	}

//    public int addition(var a, var b) {
//        return a + b;
//    }
}
