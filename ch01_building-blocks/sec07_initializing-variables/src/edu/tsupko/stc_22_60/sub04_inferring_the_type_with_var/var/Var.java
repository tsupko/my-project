package edu.tsupko.stc_22_60.sub04_inferring_the_type_with_var.var;

public class Var {
	public void var() {
		var var = "var";
	}

	public void Var() {
		Var var = new Var();
	}
}
