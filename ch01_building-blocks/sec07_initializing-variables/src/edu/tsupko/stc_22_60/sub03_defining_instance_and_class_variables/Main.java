package edu.tsupko.stc_22_60.sub03_defining_instance_and_class_variables;

public class Main {
    static double classVariable; // значение по умолчанию `0.0`
    int instanceVariable; // значение по умолчанию `0`

    public static void main(String[] args) {
        // к переменной класса можно получить доступ без создания экземпляра
        System.out.println("classVariable = " + classVariable);
        // к переменной экземпляра можно получить доступ только при создании экземпляра
        System.out.println("instanceVariable = " + new Main().instanceVariable);
        // к переменной класса также можно получить доступ с помощью экземпляра, но не рекомендуется
        System.out.println("classVariableViaObject = " + new Main().classVariable);

        // даже если ссылка не указывает ни на какой объект, всё равно можно получить переменную класса
        Main main = null;
        System.out.println("classVariableUsingNullReference = " + classVariable);
        // но при обращении к переменной экземпляра по нулевой ссылке получим `NullPointerException`
        System.out.println("instanceVariableUsingNullReference = " + main.instanceVariable);

        Main main1 = new Main();
        main1.instanceVariable = 1;
        classVariable = 3.14;

        Main main2 = new Main();
        main2.instanceVariable = 2;
        classVariable = 2.7;

        System.out.println("main1 = " + main1.instanceVariable);
        System.out.println("main2 = " + main2.instanceVariable);
        System.out.println("main1ClassVariable = " + classVariable); // 2.7
        System.out.println("main2ClassVariable = " + classVariable); // одно и то же значение
        System.out.println("рекомендуемый способ обращения к переменной класса - по имени класса: " + Main.classVariable);
    }
}
