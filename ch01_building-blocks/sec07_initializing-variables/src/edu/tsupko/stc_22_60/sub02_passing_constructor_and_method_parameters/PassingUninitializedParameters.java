package edu.tsupko.stc_22_60.sub02_passing_constructor_and_method_parameters;

public class PassingUninitializedParameters {
	public void checkAnswer() {
		boolean value;
//        findAnswer(value); // компилируется?
	}

	public void findAnswer(boolean check) {
		int answer;
		int otherAnswer;
		int onlyOneBranch;
		if (check) {
			onlyOneBranch = 1;
			answer = 1;
		} else {
			answer = 2;
		}
		System.out.println(answer);
//        System.out.println(onlyOneBranch); // компилируется?
	}
}
