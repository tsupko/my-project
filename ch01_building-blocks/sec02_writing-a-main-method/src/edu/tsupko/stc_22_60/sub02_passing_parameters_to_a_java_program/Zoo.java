package edu.tsupko.stc_22_60.sub02_passing_parameters_to_a_java_program;

/**
 * Компиляция: {@code javac Zoo.java}
 * <p>
 * Исполнение: {@code java Zoo зоопарк Казань}
 * <p>или<p>
 * Исполнение: {@code java Zoo зоопарк "Нижний Новгород"}
 */
public class Zoo {
	public static void main(String[] args) {
		System.out.println(args[0]);
		System.out.println(args[1]);
	}
}
