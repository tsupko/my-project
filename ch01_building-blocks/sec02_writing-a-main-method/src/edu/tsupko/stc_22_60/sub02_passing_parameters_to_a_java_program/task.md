1. [ ] Что будет, если вы запустите программу `sub02_passing_parameters_to_a_java_program.Zoo`
   с параметрами `зоопарк Нижний Новгород`?

2. [ ] Что будет, если вы запустите программу `sub02_passing_parameters_to_a_java_program.Zoo`
   с параметром `зоопарк`?

3. [ ] Что будет, если вы запустите программу `sub02_passing_parameters_to_a_java_program.Zoo`
   с параметрами `123 3.14`?

4. [ ] Что будет, если вы запустите программу `sub02_passing_parameters_to_a_java_program.Zoo`
   с параметрами `null null`?
