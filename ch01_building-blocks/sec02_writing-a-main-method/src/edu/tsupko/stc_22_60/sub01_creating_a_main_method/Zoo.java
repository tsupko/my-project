package edu.tsupko.stc_22_60.sub01_creating_a_main_method;

/**
 * Компиляция: {@code javac Zoo.java}
 * <p>
 * Исполнение: {@code java Zoo}
 */
public class Zoo {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    /*
    В качестве параметра методу `main()` можно передать любое действительное имя переменной в одном из следующих вариантов:
    `String[] args`     - наиболее частый вариант
    `String options[]`  - С-подобный вариант
    `String... friends` - вариант с переменным числом аргументов (об этом позже)
     */
}
