package edu.tsupko.stc_22_60.sub04_compiling_and_running_code_with_packages.packageb;

import edu.tsupko.stc_22_60.sub04_compiling_and_running_code_with_packages.packagea.ClassA;

/**
 * Компиляция:
 * <ul>
 *     <li>{@code javac packagea/ClassA.java packageb/ClassB.java}</li>
 *     <li>{@code javac packagea/*.java packageb/*.java}</li>
 * </ul>
 * <p>из каталога {@code sub04_compiling_and_running_code_with_packages}
 * <p>Исполнение: {@code java packageb.ClassB}
 */
public class ClassB {
    public static void main(String[] args) {
        ClassA a;
        System.out.println("Понятно");
    }
}
