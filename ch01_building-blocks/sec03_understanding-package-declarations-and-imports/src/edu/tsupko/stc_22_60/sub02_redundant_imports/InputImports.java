package edu.tsupko.stc_22_60.sub02_redundant_imports;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Какие есть варианты для импорта классов в данном коде?
 * <p>
 * Считайте, что оба типа {@code Files} и {@code Paths} находятся в одном и том же пакете {@code java.nio.file}.
 */
public class InputImports {
    public void read(Files files) {
        Paths.get("name");
    }
}
