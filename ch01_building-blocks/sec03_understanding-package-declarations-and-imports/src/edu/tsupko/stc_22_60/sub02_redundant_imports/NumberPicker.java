package edu.tsupko.stc_22_60.sub02_redundant_imports;

import java.util.Random;

/**
 * Сколько импортов являются лишними?
 */
public class NumberPicker {
    public static void main(String[] args) {
        Random r = new Random();
        System.out.println(r.nextInt(10));
    }
}
