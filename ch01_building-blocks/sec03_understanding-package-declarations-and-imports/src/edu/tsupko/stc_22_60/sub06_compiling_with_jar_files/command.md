Windows:

```bash
java -cp ".;C:\temp\someOtherLocation;c:\temp\myJar.jar" myPackage.MyClass
```

Linux/macOS:

```bash
java -cp ".:/tmp/someOtherLocation:/tmp/myJar.jar" myPackage.MyClass
```

Чтобы запустить программу `myPackage.MyClass` с несколькими JAR-файлами, используют команду:

```bash
java -cp "C:\temp\directoryWithJars\*" myPackage.MyClass
```

где `directoryWithJars` - каталог с JAR-файлами. При этом в classpath не будут включены JAR-файлы в подкаталогах.
