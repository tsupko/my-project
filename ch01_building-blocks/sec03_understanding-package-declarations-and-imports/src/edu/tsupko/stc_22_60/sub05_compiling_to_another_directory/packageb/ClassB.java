package edu.tsupko.stc_22_60.sub05_compiling_to_another_directory.packageb;

import edu.tsupko.stc_22_60.sub05_compiling_to_another_directory.packagea.ClassA;

/**
 * Компиляция:
 * <p>{@code javac -d classes packagea/ClassA.java packageb/ClassB.java}
 * <p>из каталога {@code sub05_compiling_to_another_directory}
 * <p>Исполнение:
 * <ul>
 *     <li>{@code java -cp classes packageb.ClassB}</li>
 *     <li>{@code java -classpath classes packageb.ClassB}</li>
 *     <li>{@code java --class-path classes packageb.ClassB}</li>
 * </ul>
 */
public class ClassB {
    public static void main(String[] args) {
        ClassA a;
        System.out.println("Понятно");
    }
}
