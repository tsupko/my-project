1. [ ] Доступен ли класс `sub01_fields_and_methods.Animal` в
   классе `sub02_comments.edu.tsupko.stc_22_60.sub01_creating_a_main_method.Zoo`? Почему?

2. [ ] Доступен ли класс `sub03_classes_and_source_files.Animal` в
   классе `sub02_comments.edu.tsupko.stc_22_60.sub01_creating_a_main_method.Zoo`? Почему?

3. [ ] Почему не возникает ошибки компиляции, если у классов одинаковое имя `Animal`
   в пакетах `sub01_fields_and_methods` и `sub03_classes_and_source_files`?
