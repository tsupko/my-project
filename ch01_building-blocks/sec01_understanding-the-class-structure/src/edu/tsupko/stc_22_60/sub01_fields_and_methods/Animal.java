package edu.tsupko.stc_22_60.sub01_fields_and_methods;

public class Animal {
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
