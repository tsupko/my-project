package edu.tsupko.stc_22_60.sub02_comments;

/**
 * многострочный комментарий Javadoc
 *
 * @author <a href="mailto:alexander.tsupko@outlook.com">Александр Цупко</a>
 */
public class Main {
	// комментарий до конца строки

	/* Много-
	 * строчный
	 * комментарий
	 */
}
