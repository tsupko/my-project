package edu.tsupko.stc_22_60.sub04_following_the_order_of_initialization;

public class Chick {
	//        {
//        System.out.println(name);
//    }
	private String name = "Пушистый";

	{
		System.out.println("установка поля");
	}

	public Chick() {
		name = "Маленький";
		System.out.println("вызов конструктора");
	}

	public static void main(String[] args) {
		Chick chick = new Chick();
		System.out.println(chick.name);
	}
}
