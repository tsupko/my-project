package edu.tsupko.stc_22_60.sub03_executing_instance_initializer_blocks;

public class Bird {
	{
		System.out.println("Снежные"); // инициализатор экземпляра
	}

	public static void main(String[] args) {
		{
			System.out.println("Перья");
		}
	}
}
