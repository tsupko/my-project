package edu.tsupko.stc_22_60.sub02_reading_and_writing_member_fields;

public class Swan {
	int numberEggs; // переменная экземпляра

	public static void main(String[] args) {
		Swan mother = new Swan();
		mother.numberEggs = 1; // установить значение поля
		System.out.println(mother.numberEggs); // считать значение поля
	}
}
