package edu.tsupko.stc_22_60.sub01_calling_constructors;

public class Main {
	public static void main(String[] args) {
		Park p = new Park();
		// Park - тип ссылки
		// p - ссылка на объект
		// new - ключевое слово, которое создаёт объект
		// Park() - вызов конструктора класса `Park`
	}
}

class Park {

}
