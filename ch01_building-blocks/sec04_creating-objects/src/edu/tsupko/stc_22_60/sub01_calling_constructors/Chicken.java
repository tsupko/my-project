package edu.tsupko.stc_22_60.sub01_calling_constructors;

public class Chicken {
	int numEggs = 12; // инициализация при объявлении
	String name;

	public Chicken() {
		name = "Duke"; // инициализация в конструкторе
	}
}
