package edu.tsupko.figures;

public interface Movable {
	void move(int x, int y);
}
