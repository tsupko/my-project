package edu.tsupko.figures;

/**
 * Представление окружности.
 *
 * @author <a href="mailto:tsupko.alexander@yandex.ru">Александр Цупко</a>
 */
public class Circle extends Ellipse implements Movable {

	public Circle(double x, double y, double radius) {
		this.x = x;
		this.y = y;
		this.width = 2 * radius;
		this.height = 2 * radius;
	}

	@Override
	public double getPerimeter() {
		return Math.PI * width;
	}

	@Override
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}
}
