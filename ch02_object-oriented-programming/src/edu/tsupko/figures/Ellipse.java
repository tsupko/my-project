package edu.tsupko.figures;

/**
 * Представление эллипса.
 *
 * @author <a href="mailto:tsupko.alexander@yandex.ru">Александр Цупко</a>
 */
public class Ellipse extends Figure {
	protected double width; // 2 большие полуоси
	protected double height; // 2 малые полуоси

	public Ellipse(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Ellipse() {
	}

	@Override
	public double getPerimeter() {
		return 0; // todo найти формулу периметра эллипса и правильно её вписать
	}

	@Override
	public double getArea() {
		return 0;
	}
}
