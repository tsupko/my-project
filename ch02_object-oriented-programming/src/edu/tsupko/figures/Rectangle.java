package edu.tsupko.figures;

/**
 * Представление прямоугольника.
 *
 * @author <a href="mailto:tsupko.alexander@yandex.ru">Александр Цупко</a>
 */
public class Rectangle extends Figure {
	protected double width;
	protected double height;

	public Rectangle(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public double getPerimeter() {
		return 2 * (width + height);
	}

	@Override
	public double getArea() {
		return 0;
	}
}
