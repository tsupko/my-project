package edu.tsupko.figures;

public abstract class Figure {
	protected double x; // абсцисса центра фигуры
	protected double y; // ордината центра фигуры

	protected Point center;

	public abstract double getPerimeter();

	public abstract double getArea(); // todo реализовать методы нахождения площади для всех фигур
}
