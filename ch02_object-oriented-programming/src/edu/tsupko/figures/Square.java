package edu.tsupko.figures;

/**
 * Представление квадрата.
 *
 * @author <a href="mailto:tsupko.alexander@yandex.ru">Александр Цупко</a>
 */
public class Square extends Rectangle implements Movable {

	public Square(double x, double y, double width, double height) {
		super(x, y, width, height);
	}

	@Override
	public double getPerimeter() {
		return super.getPerimeter();
	}

	@Override
	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}
}
