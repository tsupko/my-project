import edu.tsupko.figures.Circle;
import edu.tsupko.figures.Ellipse;
import edu.tsupko.figures.Figure;
import edu.tsupko.figures.Movable;
import edu.tsupko.figures.Rectangle;
import edu.tsupko.figures.Square;

import java.util.Random;

public class Main {
	public static void main(String[] args) {
		Square square = new Square(-5, -3, 2, 2);
		Circle circle = new Circle(-4, 2, 2);

		Rectangle rectangle = new Rectangle(1.75, 2.5, 2.5, 1);
		Ellipse ellipse = new Ellipse(-3, 1, 3, 2.4);

		Figure[] figures = new Figure[]{square, circle, rectangle, ellipse};
		for (Figure figure : figures) {
			System.out.println(figure.getPerimeter());
			System.out.println(figure.getArea());
		}

		Random random = new Random();

		Movable[] movables = new Movable[]{square, circle};
		for (Movable movable : movables) {
			int toMoveX = random.nextInt(-2, 3); // -2, -1, 0, 1, 2
			int toMoveY = random.nextInt(-2, 3); // -2, -1, 0, 1, 2
			movable.move(toMoveX, toMoveY);
			// todo добавить вывод в консоль новых координат центра и размеров
		}
	}
}
